var express = require('express');
var router = express.Router();
var mongoOp     =   require("../model/mongo");
/* GET users listing. */
router.get('/', function(req, res, next) {
  var response = {};
  mongoOp.find({},function(err,data){
    if(err) {
      response = {"error" : true,"message" : "Error fetching data"};
    } else {
      response = {"error" : false,"message" : data};
    }
    res.json(data);
  }) ;
});

router.post('/', function(req, res, next) {
  var db = new mongoOp();
  var response = {};

  db.name = req.body.name;
  db.longitude = req.body.longitude;
  db.latitude = req.body.latitude;
  db.ownerEmail = req.body.ownerEmail;
  
  db.save(function(err){
  if(err) {
      response = {"error" : true,"message" : err};
    } else {
      response = {"error" : false,"message" : "Data added"};
    }
    res.json(response);
  });
});

module.exports = router;
