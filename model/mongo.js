var mongoose = require('mongoose'),
    validator = require('node-mongoose-validator');

mongoose.connect('mongodb://localhost:27017/gymentry');
// create instance of Schema
var Schema  =   mongoose.Schema;
UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    longitude:{
        type: String,
        required: true
    } ,
    latitude:{
        type: String,
        required: true
    },
    ownerEmail:{
        type:String,
        required : true
    }
});
UserSchema.path('ownerEmail').validate(validator.isEmail(), 'Please provide a valid email address');
module.exports = mongoose.model('Products',UserSchema);